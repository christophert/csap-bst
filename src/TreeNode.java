
/*
 * Original work Copyright (c) 2013 TranIT
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/pr sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Project: 	csap-bst
 * Created:	5/22/14 4:51 PM
 *
 * Christopher Tran / CSAP
 * User: officer
 * Date: 5/22/2014
 * Time: 51
 *
 * Private Data:
 * 	Static:
 * 	int value: Holds the value of the node.
 * 	TreeNode left: holds the left node
 * 	TreeNode right: holds the right node
 * Methods:
 *  (get,set)Value: gets or sets the value of the node.
 *  (get,set)Left: gets the current left node or sets a new left node.
 *  (get,set)Right: gets the current right node or sets a new right node.
 */

public class TreeNode {
    private int value;
    private TreeNode left;
    private TreeNode right;

    public TreeNode(int n) {
        value = n;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public TreeNode getLeft() {
        return left;
    }

    public void setLeft(TreeNode left) {
        this.left = left;
    }

    public TreeNode getRight() {
        return right;
    }

    public void setRight(TreeNode right) {
        this.right = right;
    }
}
