/*
 * Original work Copyright (c) 2013 TranIT
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/pr sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Project: 	csap-bst
 * Created:	5/22/14 4:51 PM
 *
 * Christopher Tran / CSAP
 * User: officer
 * Date: 5/22/2014
 * Time: 51
 *
 * Private Data:
 *  TreeNode root: This holds the whole tree. (root node)
 * 	Static:
 * Methods:
 *  preOrder: Sorts and outputs in preorder traversal format.
 *  inOrder: Sorts and outputs in in-order traversal format.
 *  postOrder: Sorts and outputs in postorder traversal format.
 *  add():
 *  insert():
 *  delete():
 *  isLeaf():
 */

public class TreeProcess {
    TreeNode root = null;
    public TreeProcess() {}
    public void preOrder(TreeNode node) {
        if(node != null) {
            System.out.println(node.getValue());
            preOrder(node.getLeft());
            preOrder(node.getRight());
        }
    }

    public void postOrder(TreeNode node) {
        if(node != null) {
            postOrder(node.getLeft());
            postOrder(node.getRight());
            System.out.println(node.getValue());
        }
    }

    public void inOrder(TreeNode node) {
        if(node != null) {
            inOrder(node.getLeft());
            System.out.println(node.getValue());
            inOrder(node.getRight());
        }
    }

    public boolean add(int newValue) {
        if(root == null)
            root = new TreeNode(newValue);
        else
            insert(root,newValue);

        return true;
    }

    public TreeNode insert(TreeNode node, int newValue) {
        if(node!=null) {
            if(newValue<node.getValue()) {
                node.setLeft(insert(node.getLeft(), newValue));
                return node;
            }
            else if(newValue>node.getValue()) {
                node.setRight(insert(node.getRight(), newValue));
                return node;
            }
            else {
                System.out.println("Already on Tree");
            }
        }
        else
            return new TreeNode(newValue);
        return node;
    }

    public TreeNode delete(TreeNode node, int val) {
        if(node!=null) {
            if(val<node.getValue()) {
                node.setLeft(delete(node.getLeft(), val));
                return node;
            }
            else if(val>node.getValue()) {
                node.setRight(delete(node.getRight(), val));
                return node;
            }
            else {
                if(!isLeaf(node)) {
                    System.out.println("Not a leaf.");
                    return node;
                }
                else {
                    System.out.println("Deleted.");
                    return null;
                }
            }
        }
        System.out.println("Not on list.");
        return null;
    }


    public boolean isLeaf(TreeNode node) {
        if(node.getLeft() == null && node.getRight() == null)
            return true;
        return false;
    }
}
