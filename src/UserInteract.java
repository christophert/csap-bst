/*
 * Original work Copyright (c) 2013 TranIT
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/pr sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Project: 	csap-bst
 * Created:	5/22/14 4:51 PM
 *
 * Christopher Tran / CSAP
 * User: officer
 * Date: 5/22/2014
 * Time: 51
 *
 * Private Data:
 *  sentinelCheck: holds the value responsible for controlling command recursion.
 * 	Static:
 * 	    COMMANDS: Holds all valid commands for the application.
 * Methods:
 *  inputMethod(String prompt): Receives input from user.
 *  getCommand(): Prompts the user for command input.
 *  addToTree(): Prompts the user for add input and calls add()
 *  deleteFromTree(): Prompts the user for delete input and calls delete()
 *  *intCheck(): Checks if input is valid integer
 *  callCommand(): Receives input from getCommand and calls corresponding command.
 *  main(): Self-explanatory.
 *
 */

import java.io.*;

public class UserInteract {
    private boolean sentinelCheck = true;
    private TreeProcess list = new TreeProcess();
    private static String COMMANDS = "ADPIOQ";
    public UserInteract() {}

    private String inputMethod(String prompt) throws IOException {
        InputStreamReader reader = new InputStreamReader(System.in);
        BufferedReader input = new BufferedReader(reader);

        System.out.print(prompt + ": ");
        return input.readLine();
    }


    public int getCommand() throws IOException {
        char tempChar;
        String tempInput;
        do {
            System.out.println("A: Add Number\nD: Delete\nP: Preorder Traversal\nI: Inorder Traversal\nO: Postorder Traversal\nQ: Exit");
            tempInput = inputMethod("Enter command").toUpperCase();
            if(tempInput.equals("") || !COMMANDS.contains(tempInput)) {
                System.out.println("Incorrect command. Please try again.");
            }
        } while (tempInput.equals("") || !COMMANDS.contains(tempInput));

        tempChar = tempInput.charAt(0);
        switch(COMMANDS.indexOf(tempChar)) {
            case 0:
                return 0;
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 4;
            default:
                return 5;
        }
    }

    private void addToTree() throws IOException {
        String tempInput;
        int tempNum;
        do {
            tempInput = inputMethod("Enter number to add or type exit to exit");
            if(tempInput.equals("") && !tempInput.equals("exit") && !intCheck(tempInput)) {
                System.out.println("NaN. Please try again.");
            }
        } while (tempInput.equals("") && !tempInput.equals("exit") && !intCheck(tempInput));
        if(tempInput.equals("exit"))
            return;

        tempNum = Integer.parseInt(tempInput);
        if(list.add(tempNum)) {
            System.out.println("Number added.");
        }
    }

    private void deleteFromTree() throws IOException {
        String tempInput;
        int tempNum;
        do {
            tempInput = inputMethod("Enter number to delete or type exit to exit");
            if(tempInput.equals("") && !tempInput.equals("exit") && !intCheck(tempInput)) {
                System.out.println("NaN. Please try again.");
            }
        } while (tempInput.equals("") && !tempInput.equals("exit") && !intCheck(tempInput));
        if(tempInput.equals("exit"))
            return;
        tempNum = Integer.parseInt(tempInput);
        list.delete(list.root, tempNum);
    }

    private boolean intCheck(String s) {
        try {
            Integer.parseInt(s);
        }
        catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public void callCommand(int c) throws IOException {
        switch(c) {
            case 0:
                addToTree();
                callCommand(getCommand());
                break;
            case 1:
                deleteFromTree();
                callCommand(getCommand());
                break;
            case 2:
                list.preOrder(list.root);
                callCommand(getCommand());
                break;
            case 3:
                list.inOrder(list.root);
                callCommand(getCommand());
                break;
            case 4:
                list.postOrder(list.root);
                callCommand(getCommand());
                break;
            default:
                sentinelCheck = false;
        }
    }

    public static void main(String args[]) throws IOException {
        UserInteract intf = new UserInteract();

        intf.callCommand(intf.getCommand());
    }
}
